jQuery(document).ready(function ($) {
    if (document.querySelector('#productSlider')) {
        var sliderNameData = null;
        $(".sortable").sortable();
        $(".sortable").disableSelection();
        $(document).on('click', '.remove-image', function (e) {
            e.preventDefault();
            $(this).parent().parent().find('input').val('');
            $(this).parent().parent().hide();
        });
        $(document).on('click', '.delete-slider', function (e) {
            e.preventDefault();
            var parent = $(this).parent().parent().parent();
            parent.find('select').each(function () {
                $(this).remove();
            });
            parent.find('input').each(function () {
                $(this).val("");
                $(this).remove();
            });
            parent.find('option').each(function () {
                $(this).val("");
            });
            parent.hide();
        });

        $(document).on('change', '.categorySelect', function () {
            let catLink = $(this).children("option:selected").data('cat-slug');
            $(this).parent().parent().parent().find('.linkInput').val(catLink);
        });

        $(document).on('click', '.addProduct', function (e) {
            e.preventDefault();
            let sliderNameData = $(this).parent().parent().parent().parent().data('sliderName');
            let sliderName = $(this).parent().parent().parent().parent().find('.slider-heading').text().replace('Slider title: ','');
            let obj = $(this);
            printProduct(sliderNameData, obj, sliderName);
        });

        $(document).on('click', '.slider-heading', function(e) {
            var element = $(this).parents('.slider-wrapper').find('.content');
            console.log($(this).parents('.slider-wrapper'));
            if (element.hasClass('active')) {
                element.removeClass('active').hide(300);
            } else {
                $('#forms-container .slider-wrapper .content').hide(300);
                $(this).parents('.slider-wrapper').find('.content').show(300).addClass('active');
            }
        });

        $(document).on('click', '.saveSlider', function(e) {
            if ($('.slider-name-input').val() === '') {
                alert('Unesite ime za slajder');
            } else {
                $('#sliderForm').submit();
            }
        });
    }
});

function printProduct(sliderNameData, obj, sliderName) {
    var container = document.querySelector('.product-list');
    var productCounter = Number(container.dataset.productCount);
    jQuery.get('/back-ajax/?action=findBySku&sku=' + jQuery(obj).parent().find('input').val(), function (JSON) {
        if (JSON) {
            if ('content' in document.createElement('template')) {
                var template = document.querySelector('#image-template').content.cloneNode(true);
                template.querySelector('.product-title').innerText = JSON.title;
                template.querySelector('.image-preview').src = JSON.imageSrc;
                template.querySelector('.product-id-input').name = 'products[id][]';
                template.querySelector('.product-id-input').value = JSON.id;
                productCounter++;

                container.dataset.productCount = String(productCounter);
                container.appendChild(template);
            } else {
                alert('Your browser does not support templates');
            }
        } else {
            alert('Proizvod nije pronadjen.');
        }
    }, 'JSON');
}
