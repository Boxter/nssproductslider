<?php
/**
 *   Plugin Name: GF Product slider
 *   Plugin URI:
 *   Description: Woo Commerce product slider
 *   Author: Green friends
 *   Author URI:
 *   Version: 1.0
 */

// Prevent direct access
defined('ABSPATH') || die();

register_activation_hook(__FILE__, 'gf_sliders_install');
register_deactivation_hook(__FILE__, 'gf_sliders_uninstall');

add_action('admin_menu', function () {
    add_submenu_page('nss-panel', 'Slajder sa proizvodima', 'Slajder sa proizvodima', 'edit_pages', 'gf-product-slider', 'route', 1);
    add_menu_page('nss-panel', 'Slajder sa proizvodima', 'Slajder sa proizvodima', 'edit_pages', 'gf-product-slider', 'route', 1);
});

function route() {
    include 'html/myHeader.php';

    $route = isset($_GET['tab']) ? $_GET['tab'] : 'view';
    switch ($route) {
        case 'view':
            $sliders = getSliders();
            include 'html/view.php';

            break;

        case 'form':
            include 'html/form.php';

            break;

        case 'saveSlider':
            $sliderId = (int) $_GET['sliderId'];
            if (!$sliderId) {
                $newId = createSlider($_POST['sliderName'], $_POST['category'], $_POST['linkInput'], $_POST['products']);
                if ($newId) {
                    header('Location: ' . home_url('/wp-admin/admin.php?page=gf-product-slider&tab=form&sliderId='.$newId));
                }
            } else {
                if (updateSlider($sliderId, $_POST['sliderName'], $_POST['category'], $_POST['linkInput'], $_POST['products'])) {
                    header('Location: ' . home_url('/wp-admin/admin.php?page=gf-product-slider&tab=form&sliderId='.$sliderId));
                }
            }

            break;

    }
}

add_action( 'admin_enqueue_scripts', function ()  {
//    wp_enqueue_script( 'gfSliderJs', '/wp-content/plugins/gf-product-slider/js/slider.js', []);
    wp_enqueue_style( 'gfSliderCss', '/wp-content/plugins/gf-product-slider/css/slider.css', []);
} );

function createSlider($title, $catId, $url, $items) {
    global $wpdb;
    $tableName = $wpdb->prefix . 'gf_slider';
    $items = serialize($items['id']);
    $sql = "INSERT INTO {$tableName} VALUES (null, '{$title}', NOW(), '{$catId}', '{$url}', '{$items}')";
    if (!$wpdb->query($sql)) {
        var_dump($wpdb->last_error);
        die();
    }
    return $wpdb->insert_id;
}

function updateSlider($sliderId, $title, $catId, $url, $items) {
    global $wpdb;
    $tableName = $wpdb->prefix . 'gf_slider';
    $items = serialize($items['id']);
    $sql = "UPDATE {$tableName} SET title = '{$title}', updatedAt = NOW(), categoryId = {$catId}, url = '{$url}', items = '{$items}'
      WHERE slideId = {$sliderId}";
    if (!$wpdb->query($sql)) {
        var_dump($wpdb->last_error);
        die();
    }
    return true;
}

function getSlider($id) {
    global $wpdb;
    $tableName = $wpdb->prefix . 'gf_slider';
    return $wpdb->get_results("SELECT * FROM {$tableName} WHERE slideId = {$id}");
}

function getSliders() {
    global $wpdb;
    $tableName = $wpdb->prefix . 'gf_slider';
    return $wpdb->get_results("SELECT * FROM {$tableName}");
}

function gf_sliders_install() {
    global $wpdb;
    $db_version = '0.1';
    require_once(ABSPATH . 'wp-admin/includes/upgrade.php');

    $tableName = $wpdb->prefix . 'gf_slider';
    $charsetCollate = $wpdb->get_charset_collate();
    $sql = "CREATE TABLE {$tableName} (
            sliderId mediumint(9) NOT NULL AUTO_INCREMENT,
            title VARCHAR(255),
            updatedAt TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
            categoryId mediumint(9) NOT NULL,
            url VARCHAR(255) NOT NULL,
            items TEXT NOT NULL,
            PRIMARY KEY  (slideId)
        ) $charsetCollate;";
    dbDelta($sql);

    add_option('db_version', $db_version);
}

function gf_sliders_uninstall()
{
    global $wpdb;
    $tableName = $wpdb->prefix . 'gf_slider';
    $wpdb->query("DROP TABLE {$tableName}");
}